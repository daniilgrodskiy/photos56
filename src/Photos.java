/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
import javafx.fxml.FXMLLoader;
import javafx.stage.*;
import javafx.scene.*;
import javafx.application.*;
import model.Current;
import model.User;

/**
 * Main class that launches the photos application
 */
public class Photos extends Application {
    /**
     * Current user of the session
     */
    User user;

    /**
     * Launch program
     * @param args no command line input
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Starts the application
     * @param primaryStage Primary window to start the program
     * @throws Exception If stage fails
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/LoginPage.fxml"));
        Parent mainPageScene = loader.load();

        // Find the controller and call initData method
//        LoginPage controller = loader.getController();

        // Set the scene
        primaryStage.setScene(new Scene(mainPageScene, 800, 500));
        primaryStage.setTitle("Photos");
        primaryStage.show();
    }

    /**
     * Stops the application
     * @throws Exception If closing the program fails
     */
    @Override
    public void stop() throws Exception {
        System.out.println("Stage is closing");
        // Save file

        if (Current.USER == null) {
            System.out.println("NO USER!");
        } else {
            User.write(Current.USER);
            System.out.println("SAVING SESSION :)");
        }

    }
}
