/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package model;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Class that gets and sets up photos pretaining to a user
 */
public class Photo implements Serializable {
    /**
     * Path to this photo object
     */
    private String photoPath;
    /**
     * Caption of this photo object
     */
    private String caption;
    /**
     * List of the albums beloning to current user
     */
    private List<Album> albums;
    /**
     * Tags belonging to this photo object
     */
    private List<Tag> tags;

    /**
     * Constructor that initializes new photo
     * @param photoPath Direcotry path of photo
     * @param caption Photos caption
     * @param albums Albums pertaning to current user
     * @param tags Tags of all photos belonging to current user
     */
    public Photo(String photoPath, String caption, List<Album> albums, List<Tag> tags) {
        this.photoPath = photoPath;
        this.caption = caption;
        this.albums = albums;
        this.tags = tags;
    }

    // ALBUMS

    /**
     * Gets albums of the user
     * @return albums of the user
     */
    public List<Album> getAlbums() {
        return albums;
    }

    /**
     * Sets the albums of the users
     * @param albums Albums of the user
     */
    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    /**
     * Removes selected album
     * @param album Selected album
     */
    public void removeAlbum(Album album) { this.albums.remove(album); }

    /**
     * Adds an album to users data
     * @param album Album being added
     */
    public void addAlbum(Album album) {
        this.albums.add(album);
    }


    // PHOTO PATH

    /**
     * Gets directory path to each photo
     * @return photo path
     */
    public String getPhotoPath() {
        return photoPath;
    }

    /**
     * Sets photo path
     * @param photoPath Photo's path
     */
    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    // CAPTION

    /**
     * Gets caption of photo
     * @return Caption of the photo
     */
    public String getCaption() {
        return caption;
    }

    /**
     * Sets caption of current photo
     * @param caption Caption we are giving to a photo
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }


    // TAGS

    /**
     * Gets tags belonging to current user
     * @return the current tags belonging to the current user
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     * Sets tags of the current user
     * @param tags Sets tags
     */
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    /**
     * removes tags of the current user
     * @param tag Tag we are removing
     */
    public void removeTag(Tag tag) {
        this.tags.remove(tag);
    }

    /**
     * Adds tag to tags of current user
     * @param tag Tag we are adding
     */
    public void addTag(Tag tag) {
        this.tags.add(tag);
    }

    // TIME

    /**
     * Gets time photo is created
     * @return date the photo was inserted
     * @throws Exception Fail to get current time
     */
    public Date getTime() throws Exception {
        Path file = Paths.get(this.photoPath);
        BasicFileAttributes attr = Files.readAttributes(file, BasicFileAttributes.class);
        return new Date(attr.lastModifiedTime().toMillis());
    }

    // TO STRING

    /**
     * Stringifys caption
     * @return caption string
     */
    @Override
    public String toString() {
        return caption;
    }
}
