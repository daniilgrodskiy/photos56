/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package model;
import java.io.Serializable;

/**
 * Class that defines the types of tags
 */
public enum TagType implements Serializable {
    /**
     * If tag is of type single
     */
    SINGLE,
    /**
     * If the current tag is of type multiple
     */
    MULTIPLE
}