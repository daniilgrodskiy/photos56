/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package model;

/**
 * Class that gets the data of the current user
 */
public class Current {
    /**
     * Current User
     */
    static public User USER;
    /**
     * Currently selected album
     */
    static public Album ALBUM;
    /**
     * Currently selected photo
     */
    static public Photo PHOTO;
    /**
     * Currently held tags
     */
    static public Tag TAG;
}
