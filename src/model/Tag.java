/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package model;

import java.io.Serializable;
import java.util.List;


/**
 * Class that gets and sets up tags pretaining to a user
 */
public class Tag implements Serializable {
    /**
     * Type of this tag
     */
    private TagType type;
    /**
     * Name of this tag
     */
    private String name;
    /**
     * Values corresponding to this tag
     */
    private List<String> values;

    /**
     * Constructor to create new tag
     * @param type Type of tag
     * @param name Name of tag
     * @param values Values of tag
     */
    public Tag(TagType type, String name, List<String> values) {
        this.type = type;
        this.name = name;
        this.values = values;
    }

    // Constructor used for User

    /**
     * Constructor used for user
     * @param type Type of tag
     * @param name Name of tag
     */
    public Tag(TagType type, String name) {
        this.type = type;
        this.name = name;
    }

    /**
     * Adds a new value to tag, only if tag's type is of type TagType.MULTIPLE
     * @param value value which you want to add to tag's value list
     * @return true if tag's type is of type TagType.MULTIPLE, false otherwise
     */
    boolean addValue(String value) {
        if (this.type == TagType.MULTIPLE) {
            values.add(value);
            return true;
        }
        return false;
    }

    /**
     * Gets tag type
     * @return tag type
     */
    public TagType getType() {
        return type;
    }

    /**
     * Sets tag type
     * @param type Tag type
     */
    public void setType(TagType type) {
        this.type = type;
    }

    /**
     * Gets name of tag
     * @return tag name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name of tag
     * @param name Sets name of the current tag
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get tag values
     * @return tag values
     */
    public List<String> getValues() {
        return values;
    }

    /**
     * Sets tag values
     * @param values tag values
     */
    public void setValues(List<String> values) {
        this.values = values;
    }

    /**
     * Stringifys tags
     * @return Tag string
     */
    @Override
    public String toString() {
        String stringForm = name;

        if (this.values != null) {
            if (this.getType() == TagType.MULTIPLE) {
                stringForm +=  " : " +  values;
            } else {
                stringForm +=  " : " + this.values.get(0);
            }
        }
        return stringForm;
    }
}
