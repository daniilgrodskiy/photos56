/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class that gets and sets up albums pretaining to a user
 */
public class Album implements Serializable {

    /**
     * Name of the album
     */
    private String name;

    /**
     * Stringifies number of photos
     * @return String of number of photos
     */
    @Override
    public String toString() {
        return name + "\n Number of photos: " + getPhotos().size();
    }

    /**
     * Constructor to make a new album
     * @param name album name
     */
    public Album(String name) {
        this.name = name;
    }

    /**
     * Gets album name
     * @return album name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets number of photos in an album
     * @return number of photos
     */
    public int getPhotosNumberOfPhotos() {
        int count = 0;
        // Go through all the photos and find if this album is in its list
        for (Photo p : Current.USER.getPhotos()) {
            for (Album a : p.getAlbums()) {
                if (a.getName().equals(this.getName())) {
                    // Album was found
                    count++;
                    break;
                }
            }
        }
        return count;
    }

    /**
     * Gets photos in an album
     * @return photos in current album
     */
    public List<Photo> getPhotos() {
        List<Photo> photos = new ArrayList<>();
        // Go through all the photos and find if this album is in its list
        for (Photo p : Current.USER.getPhotos()) {
            for (Album a : p.getAlbums()) {
                if (a.getName().equals(this.getName())) {
                    // Album was found
                    photos.add(p);
                    break;
                }
            }
        }
        return photos;
    }

    /**
     * Get dates the photos were posted
     * @return dates of each photo
     * @throws Exception If we fail getting the current date
     */
    public String getTimeFrame() throws Exception {
        Date earliestDate = new Date();
        Date latestDate = new Date();

        // Go through all the photos and find if this album is in its list
        for (Photo p : Current.USER.getPhotos()) {
            for (Album a : p.getAlbums()) {
                if (a.getName().equals(this.getName())) {
                    // Album was found
                    if (p.getTime().before(earliestDate)) {
                        earliestDate = p.getTime();
                    }
                    if (p.getTime().after(latestDate)) {
                        latestDate = p.getTime();
                    }
                    break;
                }
            }
        }

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        return df.format(earliestDate) + " to " + df.format(latestDate);
    }

    /**
     * Sets name of album
     * @param name album name
     */
    public void setName(String name) {
        this.name = name;
    }

}
