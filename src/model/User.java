/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package model;

import java.io.*;
import java.util.List;

/**
 * Class that gets and sets up the current user and their data
 */
public class User implements Serializable {
    /**
     * Username of current user
     */
    private String username;
    /**
     * Albums belonging to current user
     */
    private List<Album> albums;
    /**
     * Photos belonging to current user
     */
    private List<Photo> photos;
    /**
     * Tags belonging to current user
     */
    private List<Tag> tags;

    /**
     * Constructor to create a user
     * @param username Username
     * @param albums Albums created by user
     * @param photos Photos in each of this users albums
     * @param tags Tags associated with the uersphotos
     */
    public User(String username, List<Album> albums, List<Photo> photos, List<Tag> tags) {
        this.username = username;
        this.albums = albums;
        this.photos = photos;
    }

    /**
     * Get username
     * @return username
     */
    public String getUsername() {
        return username;
    }

    // ALBUMS

    /**
     * Gets the users albums
     * @return current user albums
     */
    public List<Album> getAlbums() {
        return albums;
    }

    /**
     * Sets albums of the users
     * @param albums users photo albums
     */
    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    /**
     * Adds photo album to the user
     * @param album Album name
     */
    public void addAlbum(Album album) {
        this.albums.add(album);
    }

    /**
     * Removes the selected album
     * @param index index of the album selected
     */
    public void removeAlbumAt(int index) {
        Album removedAlbum = this.albums.get(index);

        this.photos.removeIf(p -> {
            if (p.getAlbums().contains(removedAlbum)) {
                p.removeAlbum(removedAlbum);
            }
            return p.getAlbums().size() == 0;
        });

        this.albums.remove(index);
    }

    /**
     * Renames selected album
     * @param oldAlbum Old album name
     * @param newName New album name
     */
    public void renameAlbum(Album oldAlbum, String newName) {
        for (Album userAlbum : this.getAlbums()) {
            if (userAlbum.getName().equals(oldAlbum.getName())) {
                // We found the album in User
                for (Photo p : this.photos) {
                    for (Album photoAlbum : p.getAlbums()) {
                        // We found a photo that has this album, so change its name
                        if (photoAlbum.getName().equals(oldAlbum.getName())) {
                            photoAlbum.setName(newName);
                            break;
                        }
                    }
                }
                userAlbum.setName(newName);
                return;
            }
        }
    }

    // PHOTOS

    /**
     * Get users photos
     * @return users photos
     */
    public List<Photo> getPhotos() {
        return photos;
    }

    /**
     * Adds a photo
     * @param photo Photo object
     */
    public void addPhoto(Photo photo) {
        this.photos.add(photo);
    }

    /**
     * Removes photo
     * @param index index of selected photo to remove
     */
    public void removePhotoAt(int index) {
        this.photos.remove(index);
    }

    /**
     * Sets the user photos
     * @param photos Users photos
     */
    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    // TAGS

    /**
     * Get tags pertaining to current user
     * @return the users tags
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     * Add new tag
     * @param tag Tag being added
     */
    public void addTag(Tag tag) {
        this.tags.add(tag);
    }

    /**
     * Sets the users tags
     * @param tags users tags
     */
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    // Serialization methods

    /**
     * Serialize the users data
     * @param user Current user
     * @throws IOException If we fail to write the data of the current user
     */
    public static void write(User user) throws IOException {
        String storeFile = "src/model/data/" + user.username + ".dat";
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(storeFile));
        oos.writeObject(user);
    }

    /**
     * Gets all the data of the current user
     * @param username Name of the current user
     * @return All of the current users data
     * @throws IOException If we fail to read the date of the current user
     * @throws ClassNotFoundException If we cant find the current user
     */
    public static User readFile(String username) throws IOException, ClassNotFoundException {
        String storeFile = "src/model/data/" + username + ".dat";
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(storeFile));
        return (User) ois.readObject();
    }
}
