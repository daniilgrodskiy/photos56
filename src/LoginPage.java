/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.Album;
import model.Current;
import model.Photo;
import model.User;
import utils.Navigator;
import widgets.ErrorBox;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Sets up and handle the functionality of the login page
 */
public class LoginPage {
    @FXML private TextField usernameField;

    /**
     * Logs in the user
     * @param event login button click
     * @throws Exception If button input fails
     */
    public void login(ActionEvent event) throws Exception {
        System.out.println("User was found: " + usernameField.getText());

        if (usernameField.getText().equals("admin")) {
            Navigator.navigateToAdminPage(event);
            return;
        }
        try {
            Current.USER = User.readFile(usernameField.getText());
        } catch (Exception e) {
            System.out.println("This user was not found!");
            ErrorBox.display("This user was not found");
            return;
        }

        Navigator.navigateToAlbumsPage(event);
    }

}
