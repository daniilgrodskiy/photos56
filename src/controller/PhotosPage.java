/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import model.Photo;
import model.Current;
import model.Tag;
import utils.Navigator;
import widgets.AlertBox;
import widgets.ErrorBox;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.SimpleFormatter;

/**
 * Sets up and handle the functionality of the photo page
 */
public class PhotosPage {
    @FXML private ListView<Photo> photosListView;
    @FXML private Button logOutButton;
    @FXML private Button addButton;
    @FXML private Button deleteButton;
    @FXML private Button editButton;
    @FXML private ImageView photoImageView;
    @FXML private Text captionText;
    @FXML private Text timeText;
    @FXML private Text albumNameText;

    private int selectedPhotoIndex = 0;
    private ObservableList<Photo> photosList;

    @FXML private ListView<Tag> tagsListView;
    private ObservableList<Tag> tagsList;

    /**
     * Initializes data for the photos page
     * @throws Exception If data initialization fails
     */
    public void initData() throws Exception {
        // Fill ListView with data
        photosList = FXCollections.observableArrayList(Current.ALBUM.getPhotos());
        photosListView.setItems(photosList);

        // Initialize JavaFX components
        photoImageView.setImage(null);
        captionText.setText("");
        timeText.setText("");
        albumNameText.setText(Current.ALBUM.getName() + " Photos");

        photosListView.getSelectionModel().selectedIndexProperty().addListener((abs, oldVal, newVal) -> {
            try {
                selectPhoto();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        photosListView.getSelectionModel().select( 0);

        tagsList = FXCollections.observableArrayList(Current.PHOTO == null ? new ArrayList<Tag>() : Current.PHOTO.getTags());
        tagsListView.setItems(tagsList);
        tagsListView.setFocusTraversable( false );

    }

    /**
     * Handles submit button click
     * @param event Submit button click
     * @throws Exception If button input fails
     */
    public void handleSlideshowButton(ActionEvent event) throws Exception {
        if (Current.ALBUM.getPhotos().size() == 0) {
            ErrorBox.display("There are no photos to show!");
        } else {
            Navigator.navigateToSlideshowPage(event);
        }
    }

    /**
     * Gets selected photo
     * @throws Exception If it fails to get slected index
     */
    public void selectPhoto() throws Exception {
        if (photosList.size() > 0) {
            photoImageView.setVisible(true);
            selectedPhotoIndex = photosListView.getSelectionModel().selectedIndexProperty().intValue();
            Photo selectedPhoto = photosList.get(selectedPhotoIndex);

            // Change properties
            captionText.setText(selectedPhoto.getCaption());
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy 'at' hh:mm aaa");
            timeText.setText(df.format(selectedPhoto.getTime()));
            InputStream stream = new FileInputStream(selectedPhoto.getPhotoPath());
            photoImageView.setImage(new Image(stream));
            Current.PHOTO = photosList.get(selectedPhotoIndex);

            tagsList = FXCollections.observableArrayList(Current.PHOTO.getTags());
            tagsListView.setItems(tagsList);
        }
    }

    // BUTTON METHODS

    /**
     * Transfers selected photo
     * @param event Transfer button click
     * @throws Exception If button input fails
     */
    public void transferPhoto(ActionEvent event) throws Exception {
        Object node = event.getSource();
        Button button = (Button) node;
        // Checks if "edit" button was clicked and if a album is selected
        Navigator.navigateToTransferPage(event, button.getText().equals("Copy"));
    }

    /**
     * Handles back button
     * @param event bakc button click
     * @throws Exception If button input fails
     */
    public void handleBackButton(ActionEvent event) throws Exception {
        // Navigate back to main page
        Navigator.navigateToAlbumsPage(event);
    }

    /**
     * Logs the user out
     * @param event Log out button click
     * @throws Exception If button input fails
     */
    public void logOut(ActionEvent event) throws Exception {
        Navigator.navigateToLoginPage(event);
    }

    /**
     * Deletes selected photo
     * @throws Exception If NULL is fetched from delete
     */
    public void deletePhoto() throws Exception {
        if (photosList.size() == 0) {
            ErrorBox.display("There are no photos to delete.");
            return;
        }

        final boolean shouldDelete = AlertBox.display("Delete Photo","Are you sure you want to delete this photo?");

        if (shouldDelete) {
            // Getting user photos and deleting at selectedPhotoIndex
            Current.USER.removePhotoAt(selectedPhotoIndex);

            photosList.remove(selectedPhotoIndex);

            if (photosList.size() > 0) {
                if (selectedPhotoIndex >= photosList.size()) {
                    photosListView.getSelectionModel().select(selectedPhotoIndex - 1);
                } else {
                    photosListView.getSelectionModel().select(selectedPhotoIndex);
                }
            } else {
                // No more photos!
                photoImageView.setImage(null);
                captionText.setText("");
                timeText.setText("");
            }
        }
    }

    /**
     * Edits selected photo
     * @param event Edit photo button click
     * @throws IOException If button input fails
     */
    public void editPhoto(ActionEvent event) throws IOException {
        //Gets source of action event
        Object node = event.getSource();
        Button button = (Button) node;
        System.out.println(button.getText());
        // Checks if "edit" button was clicked and if a photo is selected
        if (button.getText().equals("Edit")) {
            if (photosList.size() == 0) {
                ErrorBox.display("There are no photos to edit.");
                return;
            }
            Current.PHOTO = photosList.get(selectedPhotoIndex);
            Navigator.navigateToEditPhotoPage(event);
            return;
        }
        // If edit wasn't clicked then add photo is called
        Current.PHOTO = null;
        Navigator.navigateToEditPhotoPage(event);
    }



}
