package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.User;
import model.Current;
import model.User;
import utils.Navigator;
import widgets.AlertBox;
import widgets.ErrorBox;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class AdminPage {
    @FXML private ListView<User> usersListView;
    @FXML private Button logOutButton;
    @FXML private TextField newUserField;

    private int selectedUserIndex = 0;
    private ObservableList<User> usersList;

    private User selectedUser;

    /**
     * Used to populate users array and show all users
     * @throws Exception if failure occurs
     */
    public void initData() throws Exception {
        // Fill ListView with data

        List<User> users = new ArrayList<>();

        // Add users
        File dir = new File("src/model/data");
        System.out.println(dir.getAbsoluteFile().getPath());
        File[] directoryListing = dir.listFiles();

        System.out.println(Arrays.toString(directoryListing));

        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (child.getPath().contains(".dat")) {
                    String username = child.getName().split(".dat")[0];
                    users.add(User.readFile(username));
                    User.write(User.readFile(username));
                }
            }
        }

        usersList = FXCollections.observableArrayList(users);

        usersListView.setItems(usersList);

        usersListView.getSelectionModel().selectedIndexProperty().addListener((abs, oldVal, newVal) -> selectUser());
        usersListView.getSelectionModel().select( 0);
    }

    /**
     * Controls which user is selected
     */
    public void selectUser() {
        if (usersList.size() > 0) {
            selectedUserIndex = usersListView.getSelectionModel().selectedIndexProperty().intValue();
            selectedUser = usersList.get(selectedUserIndex);
        }
    }

    // BUTTON METHODS

    /**
     * Handles logging out admin
     * @param event is the action event
     * @throws Exception if failure occurs
     */
    public void logOut(ActionEvent event) throws Exception {
        Navigator.navigateToLoginPageForAdmin(event);
    }

    /**
     * Deletes selectedUser and file
     * @throws Exception if failure occurred
     */

    public void deleteUser() throws Exception {
        if (usersList.size() == 0) {
            ErrorBox.display("There are no users to delete.");
            return;
        }

        final boolean shouldDelete = AlertBox.display("Delete User","Are you sure you want to delete this user?");

        if (shouldDelete) {
            // Getting user users and deleting at selectedUserIndex

            // TODO: Delete dat file
            // Add users
            File user = new File("src/model/data/" + selectedUser.getUsername() + ".dat");
            System.out.println(user.getPath());
            if (user.delete()) {
                System.out.println("Deleted the user: " + user.getName());
            } else {
                System.out.println("Failed to delete the file.");
            }

            usersList.remove(selectedUserIndex);

            if (usersList.size() > 0) {
                if (selectedUserIndex >= usersList.size()) {
                    usersListView.getSelectionModel().select(selectedUserIndex - 1);
                } else {
                    usersListView.getSelectionModel().select(selectedUserIndex);
                }
            }
        }
    }

    /**
     * Adds user based on userTextField value
     * @param event is the action event
     * @throws IOException if file is not found
     */
    public void addNewUser(ActionEvent event) throws IOException {
        if (newUserField.getText().equals("")) {
            ErrorBox.display("You must give a name!");
            return;
        }

        File dir = new File("src/model/data");
        File[] directoryListing = dir.listFiles();

        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (child.getPath().contains(".dat")) {
                    String username = child.getName().split(".dat")[0];
                    if (username.equals(newUserField.getText())) {
                        ErrorBox.display("This user already exists!");
                        return;
                    }
                }
            }
        }

        User newUser = new User(newUserField.getText(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        User.write(newUser);
        usersList.add(newUser);

        newUserField.setText("");
    }

}
