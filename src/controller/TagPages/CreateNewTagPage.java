/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package controller.TagPages;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.Current;
import model.TagType;
import utils.*;
import model.Tag;
import widgets.ErrorBox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Creates a new tag for the current user
 */
public class CreateNewTagPage {
    // Components
    @FXML private TextField nameField;
//    @FXML private TextField valueField;
    @FXML private Button backButton;
    @FXML private Button submitButton;
    @FXML private ChoiceBox<String> valuesChoiceBox;

    /**
     * Initializes data for create new tag page
     */
    public void initData() {
        valuesChoiceBox.setItems(FXCollections.observableArrayList(new ArrayList<>(Arrays.asList("Single", "Multiple"))));
        valuesChoiceBox.setValue("Single");
    }

    /**
     * Handles back button click
     * @param event Back button click
     * @throws Exception If button input fails
     */
    public void handleBackButton(ActionEvent event) throws Exception {
        // Navigate back to main page
        Navigator.navigateToEditPhotoPage(event);
    }

    /**
     * Handles submit button click
     * @param event Submit button click
     * @throws Exception If button input fails
     */
    public void handleSubmitButton(ActionEvent event) throws Exception {

        // CHECKING NAME
        if (nameField.getText().equals("")){
            ErrorBox.display("Please enter a tag name!");
            return;
        }
        // ADDING/SAVING SONG
        List<Tag> tags = Current.USER.getTags() == null ? new ArrayList<>() : Current.USER.getTags();
        TagType tagType;

        if (valuesChoiceBox.getValue().equals("Single")) {
            tagType = TagType.SINGLE;
        } else {
            tagType = TagType.MULTIPLE;
        }

        Tag newTag = new Tag(tagType, nameField.getText());

        // ERROR CHECK
        for (Tag tag : tags) {
            if (tag.getName().equals(newTag.getName())) {
                // ERROR
                ErrorBox.display("This tag name already exists.");
                return;
            }
        }

        // Adding a new tag
        tags.add(newTag);
        Current.USER.setTags(tags);
        handleBackButton(event);
    }

}
