/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package controller.TagPages;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import model.Current;
import model.Photo;
import model.TagType;
import utils.*;
import model.Tag;
import widgets.ErrorBox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Edits the tags of the current user
 */
public class EditTagPage {
    // Components
    /**
     *
     */
    @FXML private TextField valueField;
    @FXML private Button backButton;
    @FXML private Button submitButton;
    @FXML private Text tagNameText;
    @FXML private Text tagTypeText;

    /**
     * Initializes data for edit tag page
     */
    public void initData() {
        tagNameText.setText(Current.TAG.getName());
        tagTypeText.setText(Current.TAG.getType() == TagType.SINGLE ? "Single Value" : "Multiple Value");

        String value = Current.TAG.getValues().get(0);

        if (Current.TAG.getType() == TagType.SINGLE) {
            value = Current.TAG.getValues().get(0);
            valueField.setText(value);
        } else {
            for (int i = 1; i < Current.TAG.getValues().size(); i++) {
                value += "; " + Current.TAG.getValues().get(i);
            }
            valueField.setText(value);
        }
    }

    /**
     * Handles back button click
     * @param event Back button click
     * @throws Exception If button input fails
     */
    public void handleBackButton(ActionEvent event) throws Exception {
        // Navigate back to main page
        Navigator.navigateToEditPhotoPage(event);
    }

    /**
     * Handles submit button click
     * @param event Submit button click
     * @throws Exception If button input fails
     */
    public void handleSubmitButton(ActionEvent event) throws Exception {
        // CHECKING NAME
        if (valueField.getText().equals("")){
            ErrorBox.display("Please enter a tag value!");
            return;
        }

        String[] valuesArray = valueField.getText().split(";");

        // Update values
        ArrayList<String> values = new ArrayList<>();

        if (Current.TAG.getType() == TagType.SINGLE) {
            values.add(valueField.getText());
        } else {
            values = new ArrayList<>(Arrays.asList(valuesArray));
        }

        // Adding a new tag
        Tag newTag = new Tag(Current.TAG.getType(), Current.TAG.getName(), values);

        List<Tag> tags = Current.PHOTO.getTags();
        tags.remove(Current.TAG);
        Current.PHOTO.addTag(newTag);
        handleBackButton(event);
    }

}
