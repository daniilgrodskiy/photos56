/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package controller.TagPages;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import model.Current;
import model.Photo;
import model.TagType;
import utils.*;
import model.Tag;
import widgets.ErrorBox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

/**
 * Class that handles and adds tags to a photo that have already been created
 */
public class AddExistingTagPage {
    // Components
    @FXML private TextField valueField;
    @FXML private Button backButton;
    @FXML private Button submitButton;

    @FXML private ListView<Tag> tagsListView;
    private int selectedTagIndex = 0;
    private ObservableList<Tag> tagsList;

    /**
     * Initializes data for add existing tag page
     */
    public void initData() {
        // Only show tags that haven't been added
        List<Tag> allTags = Current.USER.getTags();

        // Remove any tags from allTags that the photo already has
        for (Tag tag : Current.PHOTO.getTags()) {
            allTags.removeIf(t -> (t.getName().equals(tag.getName())));
        }

        tagsList = FXCollections.observableArrayList(allTags);
        tagsListView.setItems(tagsList);
        tagsListView.getSelectionModel().selectedIndexProperty().addListener((abs, oldVal, newVal) -> selectTag());
        tagsListView.getSelectionModel().select( 0);
    }

    /**
     * Gets index of selected tag
     */
    public void selectTag() {
        if (tagsList.size() > 0) {
            selectedTagIndex = tagsListView.getSelectionModel().selectedIndexProperty().intValue();
            Current.TAG = tagsList.get(selectedTagIndex);
        }
    }

    /**
     * Handles back button click
     * @param event Back button click
     * @throws Exception If button input fails
     */
    public void handleBackButton(ActionEvent event) throws Exception {
        // Navigate back to main page
        Navigator.navigateToEditPhotoPage(event);
    }

    /**
     * Handles submit button click
     * @param event Submit button click
     * @throws Exception If button input fails
     */
    public void handleSubmitButton(ActionEvent event) throws Exception {
        // CHECKING NAME
        if (valueField.getText().equals("")){
            ErrorBox.display("Please enter a tag value!");
            return;
        }

        String[] valuesArray = valueField.getText().split(";");

        // Update values
        ArrayList<String> values = new ArrayList<>();

        if (Current.TAG.getType() == TagType.SINGLE) {
            values.add(valueField.getText());
        } else {
            values = new ArrayList<>(Arrays.asList(valuesArray));
        }

        Tag newTag = new Tag(
            Current.TAG.getType(),
            Current.TAG.getName(),
            values
        );

        // Adding a new tag
        Current.PHOTO.addTag(newTag);
        handleBackButton(event);
    }

}
