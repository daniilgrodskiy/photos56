/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Album;
import model.Current;
import model.Tag;
import utils.*;
import model.Photo;
import widgets.AlertBox;
import widgets.ErrorBox;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Sets up and handle the functionality of the edit photo page
 */
public class EditPhotoPage {

    // Components
    @FXML private Text titleText;
    @FXML private Text photoPathText;
    @FXML private TextField captionField;
    @FXML private Button backButton;
    @FXML private Button submitButton;
    @FXML private Button selectImageButton;

    @FXML private ListView<Tag> tagsListView;
    @FXML private Button addNewTagButton;
    @FXML private Button addExistingTagButton;
    @FXML private Button createNewTagButton;
    @FXML private Button deleteTagButton;

    @FXML private HBox tagButtonsHBox;

    private int selectedTagIndex = 0;
    private ObservableList<Tag> tagsList = FXCollections.observableArrayList(new ArrayList<Tag>());

    Photo oldPhoto = null;

    /**
     * Initialize EditPhotoPage data
     */
    public void initData() {
        if (Current.PHOTO != null) {
            // Old photo
            selectImageButton.setVisible(false);
            oldPhoto = Current.PHOTO;

            titleText.setText("Edit photo");
            photoPathText.setText(Current.PHOTO.getPhotoPath());
            captionField.setText(Current.PHOTO.getCaption());

            submitButton.setText("Save");
        } else {
            // New photo
            tagButtonsHBox.setVisible(false);
            tagsListView.setVisible(false);
            titleText.setText("Add photo");
            photoPathText.setText("");
            captionField.setText("");

            submitButton.setText("Create");
        }

        // Fill ListView with data
        if (oldPhoto != null) {
            tagsList = FXCollections.observableArrayList(Current.PHOTO.getTags());
        }

        tagsListView.setItems(tagsList);
        tagsListView.getSelectionModel().selectedIndexProperty().addListener((abs, oldVal, newVal) -> selectTag());
        tagsListView.getSelectionModel().select( 0);
    }

    /**
     * Sets index of selected tag
     */
    public void selectTag() {
        if (tagsList.size() > 0) {
            selectedTagIndex = tagsListView.getSelectionModel().selectedIndexProperty().intValue();
            Current.TAG = tagsList.get(selectedTagIndex);
        }
    }

    /**
     * Gets index of selected image
     * @param event Photo page button click
     * @throws Exception If button input fails
     */
    public void selectImage(ActionEvent event) throws Exception {
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        final FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(window);
        if (file != null) {
            photoPathText.setText(file.getPath());
        }

        List<Photo> photos = Current.USER.getPhotos();

        // Check if this photo already exists
        for (Photo p : photos) {
            if (p.getPhotoPath().equals(photoPathText.getText())) {
                // This photo already exists
                captionField.setVisible(false);
                return;
            }
        }
        captionField.setVisible(true);
    }

    /**
     * Deletes currently selected tag
     */
    public void deleteTag() {
        if (tagsList.size() == 0) {
            ErrorBox.display("There are no tags to delete.");
            return;
        }

        final boolean shouldDelete = AlertBox.display("Delete Tag","Are you sure you want to delete this tag from this photo?");

        if (shouldDelete) {
            // Getting user tags and deleting at selectedTagIndex
            Current.PHOTO.removeTag(Current.PHOTO.getTags().get(selectedTagIndex));

            tagsList.remove(selectedTagIndex);

            if (tagsList.size() > 0) {
                if (selectedTagIndex >= tagsList.size()) {
                    tagsListView.getSelectionModel().select(selectedTagIndex - 1);
                } else {
                    tagsListView.getSelectionModel().select(selectedTagIndex);
                }
            }
        }
    }

    /**
     * Creates new tag
     * @param event Create tag button click
     * @throws Exception If button input fails
     */
    public void createNewTag(ActionEvent event) throws Exception {
        Navigator.navigateToCreateNewTagPage(event);
    }

    /**
     * Adds tag to list of tags
     * @param event Add tag button click
     * @throws Exception If button input fails
     */
    public void addExistingTag(ActionEvent event) throws Exception {
        List<Tag> allTags = Current.USER.getTags();

        // Remove any tags from allTags that the photo already has
        for (Tag tag : Current.PHOTO.getTags()) {
            allTags.removeIf(t -> (t.getName().equals(tag.getName())));
        }

        if (allTags.size() == 0) {
            ErrorBox.display("There are no other existing tags to add.");
            return;
        }

        Navigator.navigateToAddExistingTagPage(event);
    }

    /**
     * Edits selected tag
     * @param event Edit tag button click
     * @throws Exception If button input fails
     */
    public void editTag(ActionEvent event) throws Exception {
        List<Tag> allTags = Current.USER.getTags();

        if (allTags.size() == 0) {
            ErrorBox.display("There are no other existing tags to edit.");
            return;
        }

        Navigator.navigateToEditTagPage(event);
    }

    /**
     * Handles back button
     * @param event Back button click
     * @throws Exception If button input fails
     */
    public void handleBackButton(ActionEvent event) throws Exception {
        // Navigate back to main page
        Navigator.navigateToPhotosPage(event);
    }

    /**
     * Handles submit button
     * @param event Submit button click
     * @throws Exception If button input fails
     */
    public void handleSubmitButton(ActionEvent event) throws Exception {
        // CHECKING NAME
        if (photoPathText.getText().equals("")){
            ErrorBox.display("Please select a photo!");
            return;
        }

        // ADDING/SAVING SONG
        List<Photo> photos = Current.USER.getPhotos();

        for (Photo p : photos) {
            if (p.getPhotoPath().equals(photoPathText.getText())) {
                // This photo already exists for the user

                for (Album a : p.getAlbums()) {
                    if (a.getName().equals(Current.ALBUM.getName())) {
                        // This photo is already in this album
                        if (oldPhoto == null) {
                            // You can't add this photo (it already exists in the album)
                            ErrorBox.display("Can't add a photo that already exists in this album.");
                            return;
                        }

                        // We are editing this photo
                        Photo newPhoto = new Photo(
                            photoPathText.getText(),
                            captionField.getText(),
                            p.getAlbums(),
                            p.getTags()
                        );

                        photos.remove(p);
                        photos.add(newPhoto);
                        Current.USER.setPhotos(photos);
                        Navigator.navigateToPhotosPage(event);
                        return;
                    }
                }
                // Photo is not in this album yet, but does exist
                Photo newPhoto = new Photo(
                        photoPathText.getText(),
                        p.getCaption(),
                        p.getAlbums(),
                        p.getTags()
                );

                newPhoto.addAlbum(Current.ALBUM);
                photos.remove(p);
                photos.add(newPhoto);
                Current.USER.setPhotos(photos);
                Navigator.navigateToPhotosPage(event);
                return;
            }
        }

        // This is a new photo that doesn't exist for the user yet
        Photo newPhoto = new Photo(
            photoPathText.getText(),
            captionField.getText(),
            new ArrayList<>(Collections.singletonList(Current.ALBUM)),
            new ArrayList<>(tagsList)
        );

        photos.add(newPhoto);
        Current.USER.setPhotos(photos);
        Navigator.navigateToPhotosPage(event);
    }

}
