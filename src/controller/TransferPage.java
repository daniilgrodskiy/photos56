/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import model.Album;
import model.Current;
import model.Photo;
import model.Tag;
import utils.Navigator;
import widgets.AlertBox;
import widgets.ErrorBox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Sets up and handle the functionality of the transfer photo page
 */
public class TransferPage {
    @FXML private ListView<Album> albumsListView;
    @FXML private Button transferButton;
    @FXML private Text titleText;

    private int selectedAlbumIndex = 0;
    private ObservableList<Album> albumsList;

    private boolean isCopy;

    /**
     * Handles back button click
     * @param event Back button click
     * @throws Exception If button input fails
     */
    public void handleBackButton(ActionEvent event) throws Exception {
        // Navigate back to main page
        Navigator.navigateToAlbumsPage(event);
    }

    /**
     * Initializes data for the transfer photo page
     * @param isCopy Whether we are copying or moving
     * @throws Exception If we fail to initialize the data
     */
    public void initData(boolean isCopy) throws Exception {
        titleText.setText(isCopy ? "Copy Photo" : "Move Photo");
        transferButton.setText(isCopy ? "Copy" : "Move");

        this.isCopy = isCopy;
        List<Album> allAlbums = Current.USER.getAlbums();
        // Remove any tags from allTags that the photo already has
//        for (Album photoAlbum : Current.PHOTO.getAlbums()) {
//            allAlbums.removeIf(a -> (a.getName().equals(photoAlbum.getName())));
//        }

        // Fill ListView with data
        albumsList = FXCollections.observableArrayList(allAlbums);
        albumsListView.setItems(albumsList);

        albumsListView.getSelectionModel().selectedIndexProperty().addListener((abs, oldVal, newVal) -> selectAlbum());
        albumsListView.getSelectionModel().select( 0);
    }

    /**
     * Gets index of selected album
     */
    public void selectAlbum() {
        if (albumsList.size() > 0) {
            selectedAlbumIndex = albumsListView.getSelectionModel().selectedIndexProperty().intValue();
        }
    }

    // BUTTON METHODS

    /**
     * Handles transfer button click
     * @param event Transfer button click
     * @throws Exception If button input fails
     */
    public void transferAlbum(ActionEvent event) throws Exception {
        // Copy
        List<Photo> allPhotos = Current.USER.getPhotos();

        if (Current.ALBUM.equals(albumsList.get(selectedAlbumIndex))) {
            ErrorBox.display("You can't move or copy this photo to the same album.");
            return;
        }


        for (Photo p : allPhotos) {
            if (p.getPhotoPath().equals(Current.PHOTO.getPhotoPath())) {
                if (!this.isCopy) {
                    // Move
                    p.removeAlbum(Current.ALBUM);
                }
                p.addAlbum(albumsList.get(selectedAlbumIndex));
                Navigator.navigateToPhotosPage(event);
                return;
            }
        }
    }

}
