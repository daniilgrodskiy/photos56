/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import model.Current;
import model.Photo;
import utils.*;
import model.Album;
import widgets.ErrorBox;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles the functionality of the adit album page
 */
public class EditAlbumPage {
    private Album oldAlbum;

    // Components
    @FXML private Text titleText;
    @FXML private TextField nameField;
    @FXML private Button backButton;
    @FXML private Button submitButton;

    /**
     * Initialize AditAlbumPage data
     */
    public void initData() {
        if (Current.ALBUM != null) {
            // Old album
            oldAlbum = Current.ALBUM ;

            titleText.setText("Edit album");
            nameField.setText(Current.ALBUM.getName());

            submitButton.setText("Save");
        } else {
            // New album
            titleText.setText("Add album");
            submitButton.setText("Create");
        }
    }

    /**
     * Handles back button click
     * @param event Back button click
     * @throws Exception If button input fails
     */
    public void handleBackButton(ActionEvent event) throws Exception {
        // Navigate back to main page
        Navigator.navigateToAlbumsPage(event);
    }

    /**
     * Handles submit button click
     * @param event Back submit click
     * @throws Exception If button input fails
     */
    public void handleSubmitButton(ActionEvent event) throws Exception {
        // CHECKING NAME
        if (nameField.getText().equals("")){
            ErrorBox.display("Please enter an album name!");
            return;
        }

        // ADDING/SAVING SONG
        List<Album> albums = Current.USER.getAlbums();

        Album newAlbum = new Album(nameField.getText());

        // ERROR CHECK
        if (oldAlbum == null) {
            // Adding new album
            for (Album album : albums) {
                if (album.getName().equals(newAlbum.getName())) {
                    // ERROR
                    ErrorBox.display("This album name already exists.");
                    return;
                }
            }
        } else {
            // Editing an old album
            for (Album album : albums) {
                if (album.getName().equals(newAlbum.getName()) && !(oldAlbum.getName().equals(newAlbum.getName()))) {
                    // ERROR
                    ErrorBox.display("This album and artist already exist together.");
                    return;
                }
            }

        }

        if (oldAlbum == null) {
            // Adding a new album
            albums.add(newAlbum);
            Current.USER.setAlbums(albums);
        } else {
            // Renaming an old album
            Current.USER.renameAlbum(oldAlbum, nameField.getText());
        }

        handleBackButton(event);

    }

}
