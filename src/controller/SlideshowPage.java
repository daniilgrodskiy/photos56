/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import model.Photo;
import model.Current;
import model.Tag;
import utils.Navigator;
import widgets.AlertBox;
import widgets.ErrorBox;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.SimpleFormatter;

/**
 * Sets up and handle the functionality of the slideshow feature
 */
public class SlideshowPage {
    @FXML private ImageView photoImageView;

    @FXML private Button lastPhotoButton;
    @FXML private Button nextPhotoButton;

    private int selectedPhotoIndex = 0;

    /**
     * Initializes slideshow data
     * @throws Exception If we fail initializing data
     */
    public void initData() throws Exception {
        lastPhotoButton.setVisible(false);
        if (Current.ALBUM.getPhotos().size() == 1) {
            nextPhotoButton.setVisible(false);
        }
        // Initialize JavaFX components

        InputStream stream = new FileInputStream(Current.ALBUM.getPhotos().get(selectedPhotoIndex).getPhotoPath());
        photoImageView.setImage(new Image(stream));
    }

    // BUTTON METHODS

    /**
     * Handles the current photo viewed in the slideshow
     * @param event last photo button click
     * @throws FileNotFoundException If we cant get current photo
     */
    public void handleLastPhoto(ActionEvent event) throws FileNotFoundException {
        selectedPhotoIndex--;
        lastPhotoButton.setVisible(selectedPhotoIndex != 0);
        nextPhotoButton.setVisible(selectedPhotoIndex != Current.ALBUM.getPhotos().size() - 1);
        InputStream stream = new FileInputStream(Current.ALBUM.getPhotos().get(selectedPhotoIndex).getPhotoPath());
        photoImageView.setImage(new Image(stream));    }

    /**
     * Gets the next photo to display in the slideshow
     * @param event next photo button click
     * @throws FileNotFoundException If we cant get the next photo
     */
    public void handleNextPhoto(ActionEvent event) throws FileNotFoundException {
        selectedPhotoIndex++;
        lastPhotoButton.setVisible(selectedPhotoIndex != 0);
        nextPhotoButton.setVisible(selectedPhotoIndex != Current.ALBUM.getPhotos().size() - 1);
        InputStream stream = new FileInputStream(Current.ALBUM.getPhotos().get(selectedPhotoIndex).getPhotoPath());
        photoImageView.setImage(new Image(stream));    }

    /**
     * Handles back button to photos page
     * @param event Back button click
     * @throws Exception If button input fails
     */
    public void handleBackButton(ActionEvent event) throws Exception {
        // Navigate back to main page
        Navigator.navigateToPhotosPage(event);
    }

}
