/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package controller;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.Album;
import model.Current;
import model.Photo;
import model.User;
import utils.Navigator;
import widgets.ErrorBox;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Sets up and handle the functionality of the login page
 */
public class LoginPage {
    @FXML private TextField usernameField;

    /**
     * Logs in the user
     * @param event login button click
     * @throws Exception If button input fails
     */
    public void login(ActionEvent event) throws Exception {
        System.out.println("User was found: " + usernameField.getText());

        if (usernameField.getText().equals("admin")) {
            Navigator.navigateToAdminPage(event);
            return;
        }
        try {
            Current.USER = User.readFile(usernameField.getText());
        } catch(Exception e) {
            System.out.println("This user was not found!");
            ErrorBox.display("This user was not found");
            Current.USER = new User(usernameField.getText(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
            return;
        }

        Navigator.navigateToAlbumsPage(event);
    }

    // TODO: Start from here!
    // - Change any "add photo to some album" to "add this photo to user's List<Photo> photos" AND...
    // - ... when adding a new photo, make sure its album property is updated as well (p.addAlbum(Current.ALBUM))

    // - After restructuring and fixing everything, work on the Transfer page (move/copy) for moving and copying photos
    // - Then, work on slideshow maybe
    // - Then, TAGS :)))
    // - Then, searching for photos
    // - Then, admin login
    // - THEN I THINK IT'S JUST JAVA DOCS AND UML :DDDD

    /**
     * Adds stock album to the library
     */
    public void addStockAlbum() {
//        Photo photo1 = new Photo("/Users/daniilgrodskiy/Downloads/testing.jpg", "This is a test!");
//        List<Photo> photos = new ArrayList<>(List.of(photo1));
//        Album album = new Album("THIS IS THE ONE", photos);
//        Current.USER.addAlbum(album);
    }


}
