/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.Album;
import model.Current;
import model.Photo;
import utils.Navigator;
import widgets.AlertBox;
import widgets.ErrorBox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Sets up and handles all actions on the album page
 */
public class AlbumsPage {
    @FXML private ListView<Album> albumsListView;
    @FXML private Button logOutButton;
    @FXML private Button searchButton;
    @FXML private Button addButton;
    @FXML private Button deleteButton;
    @FXML private Button renameButton;
    @FXML private Button openButton;

    private int selectedAlbumIndex = 0;
    private ObservableList<Album> albumsList;

    /**
     * Initialize AlbumPages data
     * @throws Exception If initializing data fails
     */
    public void initData() throws Exception {
        // Fill ListView with data
        albumsList = FXCollections.observableArrayList(Current.USER.getAlbums());

        albumsListView.setItems(albumsList);

        albumsListView.getSelectionModel().selectedIndexProperty().addListener((abs, oldVal, newVal) -> selectAlbum());
        albumsListView.getSelectionModel().select( 0);
    }

    /**
     * Get index of the currently selected album
     */
    public void selectAlbum() {
        if (albumsList.size() > 0) {
            selectedAlbumIndex = albumsListView.getSelectionModel().selectedIndexProperty().intValue();
            Current.ALBUM = albumsList.get(selectedAlbumIndex);
        }
    }

    // BUTTON METHODS

    /**
     * Logout button handle
     * @param event logout button click
     * @throws Exception If logging out fails
     */
    public void logOut(ActionEvent event) throws Exception {
        Navigator.navigateToLoginPage(event);
    }

    /**
     * Search the photos of the current user
     */
    public void search() {

    }

    /**
     * Delete currents selected album
     * @throws Exception If deleting an album fails
     */
    public void deleteAlbum() throws Exception {
        if (albumsList.size() == 0) {
            ErrorBox.display("There are no albums to delete.");
            return;
        }

        final boolean shouldDelete = AlertBox.display("Delete Album","Are you sure you want to delete this album?");

        if (shouldDelete) {
            // Getting user albums and deleting at selectedAlbumIndex
            Current.USER.removeAlbumAt(selectedAlbumIndex);

            albumsList.remove(selectedAlbumIndex);

            if (albumsList.size() > 0) {
                if (selectedAlbumIndex >= albumsList.size()) {
                    albumsListView.getSelectionModel().select(selectedAlbumIndex - 1);
                } else {
                    albumsListView.getSelectionModel().select(selectedAlbumIndex);
                }
            }
        }
    }

    /**
     * Adit currently selected album
     * @param event Edit album button click
     * @throws IOException If input from button fails
     */
    public void editAlbum(ActionEvent event) throws IOException {
        //Gets source of action event
        Object node = event.getSource();
        Button button = (Button) node;
        // Checks if "edit" button was clicked and if a album is selected
        if (button.getText().equals("Rename")) {
            if (albumsList.size() == 0) {
                ErrorBox.display("There are no albums to rename.");
                return;
            }
            Current.ALBUM = albumsList.get(selectedAlbumIndex);
            Navigator.navigateToEditAlbumPage(event);
            return;
        }
        // If edit wasn't clicked then add album is called
        Current.ALBUM = null;
        Navigator.navigateToEditAlbumPage(event);
    }

    /**
     * Open the current selected album
     * @param event Open album button click
     * @throws Exception If input from button fails
     */
    public void openAlbum(ActionEvent event) throws Exception {
        // Navigate to album's photos page
        Navigator.navigateToPhotosPage(event);
    }




}
