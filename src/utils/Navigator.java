/**
 * @author Daniil Grodskiy and Christopher McKiernan
 */
package utils;

import controller.*;
import controller.TagPages.AddExistingTagPage;
import controller.TagPages.CreateNewTagPage;
import controller.TagPages.EditTagPage;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import model.Current;
import model.User;

import java.io.IOException;

/**
 * Class that handles the navigation between each page
 */
public class Navigator {

    /**
     * Navigates to the admin page
     * @param event Login button click
     * @throws IOException If button click fails
     * @throws Exception If we fail to get the admin
     */
    public static void navigateToAdminPage(ActionEvent event) throws IOException, Exception {
        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();

        // To be able to statically use this method, we must add "Navigator.class"
        loader.setLocation(Navigator.class.getResource("../view/AdminPage.fxml"));
        Parent mainPageScene = loader.load();

        // Find the controller and call initData method
        AdminPage controller = loader.getController();
        controller.initData();

        // Show scene
        // Gets the current window (avoids using 'Stage primaryStage' thing
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(mainPageScene, 800, 500));
        window.show();
    }

    /**
     * Navigates to slide show page
     * @param event Slideshow button press
     * @throws IOException If button input fails
     * @throws Exception If we cant find the slideshowp page
     */
    public static void navigateToSlideshowPage(ActionEvent event) throws IOException, Exception {
        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();

        // To be able to statically use this method, we must add "Navigator.class"
        loader.setLocation(Navigator.class.getResource("../view/SlideshowPage.fxml"));
        Parent mainPageScene = loader.load();

        // Find the controller and call initData method
        SlideshowPage controller = loader.getController();
        controller.initData();

        // Show scene
        // Gets the current window (avoids using 'Stage primaryStage' thing
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(mainPageScene, 800, 500));
        window.show();
    }

    /**
     * Navigates to transfer page
     * @param event Transer button click
     * @param isCopy Whether we are copying or moving
     * @throws IOException If button input fails
     * @throws Exception If we cant find the transfer page
     */
    public static void navigateToTransferPage(ActionEvent event, boolean isCopy) throws IOException, Exception {
        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();

        // To be able to statically use this method, we must add "Navigator.class"
        loader.setLocation(Navigator.class.getResource("../view/TransferPage.fxml"));
        Parent mainPageScene = loader.load();

        // Find the controller and call initData method
        TransferPage controller = loader.getController();
        controller.initData(isCopy);

        // Show scene
        // Gets the current window (avoids using 'Stage primaryStage' thing
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(mainPageScene, 800, 500));
        window.show();
    }

    /**
     * Navigates to edit page
     * @param event Edit button click
     * @throws IOException If button input fails
     * @throws Exception If we cant find the edit page
     */
    public static void navigateToEditTagPage(ActionEvent event) throws IOException, Exception {
        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();

        // To be able to statically use this method, we must add "Navigator.class"
        loader.setLocation(Navigator.class.getResource("../view/TagPages/EditTagPage.fxml"));
        Parent mainPageScene = loader.load();

        // Find the controller and call initData method
        EditTagPage controller = loader.getController();
        controller.initData();

        // Show scene
        // Gets the current window (avoids using 'Stage primaryStage' thing
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(mainPageScene, 800, 500));
        window.show();
    }

    /**
     * Navigates to existing tags page
     * @param event Add existing tag button click
     * @throws IOException If button input fails
     * @throws Exception If we cant find the existing tags page
     */
    public static void navigateToAddExistingTagPage(ActionEvent event) throws IOException, Exception {
        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();

        // To be able to statically use this method, we must add "Navigator.class"
        loader.setLocation(Navigator.class.getResource("../view/TagPages/AddExistingTagPage.fxml"));
        Parent mainPageScene = loader.load();

        // Find the controller and call initData method
        AddExistingTagPage controller = loader.getController();
        controller.initData();

        // Show scene
        // Gets the current window (avoids using 'Stage primaryStage' thing
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(mainPageScene, 800, 500));
        window.show();
    }

    /**
     * Navigates to create new tag page
     * @param event Create new tag button click
     * @throws IOException If button input fails
     * @throws Exception If we cant find the createnewtag page
     */
    public static void navigateToCreateNewTagPage(ActionEvent event) throws IOException, Exception {
        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();

        // To be able to statically use this method, we must add "Navigator.class"
        loader.setLocation(Navigator.class.getResource("../view/TagPages/CreateNewTagPage.fxml"));
        Parent mainPageScene = loader.load();

        // Find the controller and call initData method
        CreateNewTagPage controller = loader.getController();
        controller.initData();

        // Show scene
        // Gets the current window (avoids using 'Stage primaryStage' thing
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(mainPageScene, 800, 500));
        window.show();
    }

    /**
     * Navigate to page holding each photo
     * @param event Go to photo button click
     * @throws IOException If button input fails
     * @throws Exception If we cant find the photos page
     */
    public static void navigateToPhotosPage(ActionEvent event) throws IOException, Exception {
        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();

        // To be able to statically use this method, we must add "Navigator.class"
        loader.setLocation(Navigator.class.getResource("../view/PhotosPage.fxml"));
        Parent mainPageScene = loader.load();

        // Find the controller and call initData method
        PhotosPage controller = loader.getController();
        controller.initData();

        // Show scene
        // Gets the current window (avoids using 'Stage primaryStage' thing
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(mainPageScene, 800, 500));
        window.show();
    }

    /**
     * Navigates to edit photo page
     * @param event edit button click
     * @throws IOException If button input fails
     */
    public static void navigateToEditPhotoPage(ActionEvent event) throws IOException {
        // Used to find out if it was the "Add" or the "Edit" button that was clicked
        Object node = event.getSource();

        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();

        // To be able to statically use this method, we must add "Navigator.class"
        loader.setLocation(Navigator.class.getResource("../view/EditPhotoPage.fxml"));
        Parent mainPageScene = loader.load();

        // Find the controller and call initData method
        EditPhotoPage controller = loader.getController();
        controller.initData();

        // Show scene
        // Gets the current window (avoids using 'Stage primaryStage' thing
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(mainPageScene, 800, 500));
        window.show();
    }

    /**
     * Navigates to edit album page
     * @param event edit album button click
     * @throws IOException If button input fails
     */
    public static void navigateToEditAlbumPage(ActionEvent event) throws IOException {
        // Used to find out if it was the "Add" or the "Edit" button that was clicked
        Object node = event.getSource();

        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();

        // To be able to statically use this method, we must add "Navigator.class"
        loader.setLocation(Navigator.class.getResource("../view/EditAlbumPage.fxml"));
        Parent mainPageScene = loader.load();

        // Find the controller and call initData method
        EditAlbumPage controller = loader.getController();
        controller.initData();

        // Show scene
        // Gets the current window (avoids using 'Stage primaryStage' thing
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(mainPageScene, 800, 500));
        window.show();
    }

    /**
     * Navigates to the albums page
     * @param event Albums button click
     * @throws IOException If button input fails
     * @throws Exception If we cant find the albums page
     */
    public static void navigateToAlbumsPage(ActionEvent event) throws IOException, Exception {
        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();

        // To be able to statically use this method, we must add "Navigator.class"
        loader.setLocation(Navigator.class.getResource("../view/AlbumsPage.fxml"));
        Parent mainPageScene = loader.load();

        // Find the controller and call initData method
        AlbumsPage controller = loader.getController();
        controller.initData();

        // Show scene
        // Gets the current window (avoids using 'Stage primaryStage' thing
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(mainPageScene, 800, 500));
        window.show();
    }

    /**
     * Navigates back to the login page
     * @param event Back/logout button click
     * @throws IOException If button input fails
     * @throws Exception If we cant find the login page
     */
    public static void navigateToLoginPage(ActionEvent event) throws IOException, Exception {
        User.write(Current.USER);
        Current.USER = null;

        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();

        // To be able to statically use this method, we must add "Navigator.class"
        loader.setLocation(Navigator.class.getResource("../view/LoginPage.fxml"));
        Parent mainPageScene = loader.load();

        // Show scene
        // Gets the current window (avoids using 'Stage primaryStage' thing
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(mainPageScene, 800, 500));
        window.show();
    }

    /**
     * Navigates back to the log in page
     * @param event Log out button click
     * @throws IOException If button click fails
     * @throws Exception If we cant get loginPage
     */
    public static void navigateToLoginPageForAdmin(ActionEvent event) throws IOException, Exception {
        // FXML loader setup
        FXMLLoader loader = new FXMLLoader();

        // To be able to statically use this method, we must add "Navigator.class"
        loader.setLocation(Navigator.class.getResource("../view/LoginPage.fxml"));
        Parent mainPageScene = loader.load();

        // Show scene
        // Gets the current window (avoids using 'Stage primaryStage' thing
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(mainPageScene, 800, 500));
        window.show();
    }
}
